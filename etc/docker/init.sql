CREATE USER mysql;
CREATE DATABASE human-resource-management;
GRANT ALL PRIVILEGES ON DATABASE human-resource-management TO mysql;