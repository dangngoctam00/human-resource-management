package htq.thesis.humanresourcemanagement.dto.base;

import lombok.Data;

@Data
public class BaseResponse {
    private String code;
    private String message;
}
