package htq.thesis.humanresourcemanagement.dto.auth;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserDto {
    private Long id;
    private String username;
    private String name;
    private String email;
    private String phone;
    private String avatar;
}
