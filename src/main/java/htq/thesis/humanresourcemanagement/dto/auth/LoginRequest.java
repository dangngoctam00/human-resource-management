package htq.thesis.humanresourcemanagement.dto.auth;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class LoginRequest {
    @NotBlank(message = "Tên đăng nhập không được để trống")
    private String username;
    @NotNull(message = "Mật khẩu không được để trống")
    private String password;
}
