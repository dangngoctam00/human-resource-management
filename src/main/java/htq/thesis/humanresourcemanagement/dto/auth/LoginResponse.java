package htq.thesis.humanresourcemanagement.dto.auth;

import htq.thesis.humanresourcemanagement.dto.base.BaseResponse;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class LoginResponse extends BaseResponse {
    private String token;
    private String refreshToken;
    private UserDto user;
    private String tenant;
}
