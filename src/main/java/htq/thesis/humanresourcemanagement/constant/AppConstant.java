package htq.thesis.humanresourcemanagement.constant;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class AppConstant {
    public static final String API_PREFIX = "/api/v1";
}
