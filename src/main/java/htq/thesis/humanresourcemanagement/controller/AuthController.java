package htq.thesis.humanresourcemanagement.controller;

import htq.thesis.humanresourcemanagement.dto.auth.LoginRequest;
import htq.thesis.humanresourcemanagement.dto.auth.LoginResponse;
import htq.thesis.humanresourcemanagement.dto.auth.RegisterRequest;
import htq.thesis.humanresourcemanagement.dto.auth.RegisterResponse;
import htq.thesis.humanresourcemanagement.service.impl.AuthService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

import static htq.thesis.humanresourcemanagement.constant.AppConstant.API_PREFIX;

@Slf4j
@RestController
@RequestMapping(API_PREFIX)
@AllArgsConstructor
public class AuthController {

    private final AuthService authService;

    @PostMapping("/login")
    public ResponseEntity<LoginResponse> login(@Valid @RequestBody LoginRequest request) {
        LoginResponse loginResponse = authService.login(request);
        loginResponse.setCode(HttpStatus.OK.toString());
        return ResponseEntity.ok(loginResponse);
    }

    @PostMapping("/register")
    public ResponseEntity<RegisterResponse> register(@RequestBody RegisterRequest request) {
        authService.register(request);
        RegisterResponse response = new RegisterResponse();
        response.setCode(HttpStatus.OK.toString());
        return ResponseEntity.ok(response);
    }
}
