package htq.thesis.humanresourcemanagement.exception.auth;

public class UsernameExistedException extends RuntimeException {
    public UsernameExistedException(String username) {
        super(username + "is used");
    }
}
