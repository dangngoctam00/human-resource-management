package htq.thesis.humanresourcemanagement.mapper;

import htq.thesis.humanresourcemanagement.dto.auth.UserDto;
import htq.thesis.humanresourcemanagement.model.UserEntity;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserMapper {

    UserDto mapToUserDto(UserEntity entity);
}
