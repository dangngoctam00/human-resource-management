package htq.thesis.humanresourcemanagement.service.impl;

import htq.thesis.humanresourcemanagement.dto.auth.LoginRequest;
import htq.thesis.humanresourcemanagement.dto.auth.LoginResponse;
import htq.thesis.humanresourcemanagement.dto.auth.RegisterRequest;
import htq.thesis.humanresourcemanagement.exception.auth.IncorrectPasswordException;
import htq.thesis.humanresourcemanagement.exception.auth.UserNotFoundException;
import htq.thesis.humanresourcemanagement.exception.auth.UsernameExistedException;
import htq.thesis.humanresourcemanagement.mapper.UserMapper;
import htq.thesis.humanresourcemanagement.model.UserEntity;
import htq.thesis.humanresourcemanagement.repository.UserRepository;
import htq.thesis.humanresourcemanagement.security.CustomUserDetails;
import htq.thesis.humanresourcemanagement.security.jwt.JWTUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
public class AuthService {
    private final JWTUtils jwtUtils;
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final UserMapper userMapper;

    @Transactional(readOnly = true)
    public LoginResponse login(LoginRequest request) {
        Optional<UserEntity> userOpt = userRepository.findByUsername(request.getUsername());
        if (userOpt.isPresent()) {
            var user = userOpt.get();
            if (!passwordEncoder.matches(request.getPassword(), user.getPassword())) {
                throw new IncorrectPasswordException();
            }
            LoginResponse loginResponse = new LoginResponse();
            CustomUserDetails userDetails = new CustomUserDetails(user.getId(), request.getUsername());
            String token = jwtUtils.generateToken(userDetails);
            loginResponse.setToken(token);
            loginResponse.setUser(userMapper.mapToUserDto(user));
            loginResponse.setRefreshToken(jwtUtils.generateRefreshToken(userDetails));
            log.info("User {} login successfully.", user.getUsername());
            return loginResponse;
        } else {
            log.info("User {} not found.", request.getUsername());
            throw new UserNotFoundException();
        }
    }

    @Transactional
    public void register(RegisterRequest request) {
        Optional<UserEntity> userEntityOptional = userRepository.findByUsername(request.getUsername());
        if (userEntityOptional.isPresent()) {
            log.info("Username {} is used", request.getUsername());
            throw new UsernameExistedException(request.getUsername());
        }
        var user = UserEntity.builder()
                .username(request.getUsername())
                .password(passwordEncoder.encode(request.getPassword()))
                .avatar(request.getAvatar())
                .name(request.getName())
                .email(request.getEmail())
                .build();
        user = userRepository.save(user);
        log.info("Register user {} successfully", user);
    }
}
